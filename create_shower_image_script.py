import os
import sys, getopt
from pycaret.anomaly import *
from pycaret.classification import *

from image_db_generator import image_db

'''
input parametets:
 -h   - help
 -i   - input xlsx file with anomaly score and shower features
 -n   - number of events in file to analyse
 -c   - path to folder with sim_telarray (*.simtel.gz) files
 -o   - puth to output folder
 -t   - anomaly score threshold
 -p   - particle type: electron, gamma, proton, muon, nsb etc.
 -f   - if True use Images with anomaly_score < threshold, if False use Images with anomaly_score > threshold
 -w   - position of expected shower time index (i.e. local maximum at waveform)
 -s   - half width of integral window for an image. Since, we will save 3 images (one for R, other for G and third for B) the totla integration window is: 3x (2x s) = 6x s
'''

# read input parametets:
try:
    opts, args = getopt.getopt(sys.argv[1:], "hi:n:c:o:t:p:f:w:s:", ["input_file", "events", "input_simtel", "output_path", "threshold", "particle", "nsb", "window_position", "step"])
except getopt.GetoptError:
    print('error: create_shower_image_script.py -i <input_file> -n <events> -c <input_simtel> -o <output_path> -t <threshold> -p <particle> -f <nsb> -w <window_position> -s <step>')
    sys.exit(2)

# def parameters:
n_events_to_ana = None
window_max_position = 38
half_window = 2


for opt, arg in opts:
    if opt == '-h':
        print('create_shower_image_script.py -i <input_file> -n <events> -c <input_simtel> -o <output_path> -t <threshold> -p <particle> -f <nsb> -w <window_position> -s <step>')
        sys.exit()
    elif opt in ("-i", "--input_file"):
        input_file = arg
    elif opt in ("-n", "--events"):
        n_events_to_ana = int(arg)
    elif opt in ("-c", "--input_simtel"):
        input_simtel_path = arg
    elif opt in ("-o", "--output_path"):
        output_path = arg
    elif opt in ("-t", "--threshold"):
        threshold_anomaly = float(arg)
    elif opt in ("-f", "--nsb"):
        do_nsb = eval(arg)
    elif opt in ("-p", "particle"):
        particle_type = str(arg)
    elif opt in ("-w", "window_position"):
        window_max_position = int(arg)
    elif opt in ("-s", "step"):
        half_window = int(arg)

if os.path.exists(input_file):
    file_name_to_ana = os.path.basename(input_file)
    print('ana n events  : ', n_events_to_ana)
    print('file to start : ', file_name_to_ana)
    print('corsica files path : ', input_simtel_path)
    print('output path : ', output_path)
    print('threshold : ', threshold_anomaly)
    print('do nsb : ', do_nsb)

    start = file_name_to_ana.find('run') + 3
    end = file_name_to_ana.find('.simtel')
    run_number = int(file_name_to_ana[start:end])
    print('run_number : ', run_number)
    print('Shower max position :', window_max_position)
    print('Image half window : ', half_window)
    
    
    # create images:
    images = image_db(input_file, input_simtel_path, output_path, threshold_anomaly, particle_type, do_nsb, n_events_to_ana, peak_pos = window_max_position, step = half_window)
else:
    print('Error :: file not exist')
