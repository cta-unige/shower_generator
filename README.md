# shower_generator

This project was developed to convert [sim_telarray](https://www.mpi-hd.mpg.de/hfm/~bernlohr/sim_telarray/) output simulation files **(*.simtel.gz)** into RGB images, which can be used further for CNN models.

## Structure:

Project has 3 classes:
1. **camera_geometry** -> read *'\input\camera_CTA-LST_SiPM_Ideal_analogsum21.dat'* file, and establish the connection between camera pixels, modules and Analog Sum Trigger classter. The class has two functions: **get_module_pixel_map()** and **get_analog_sum_trigger_map()** first return dictionary with key = modul number and value = list of pixels, second with key = analog_sum_number and value = list of pixels
2. **anomaly_db_generator** -> to read sim_telarray output simulation files **(*.simtel.gz)** and create the xlsx files with shower features, and use those features to run anomaly detection algorithm (by deffoult isolation forest algorithm). As a result the last column of xlsx file is the anomaly score.
3.  **image_db_generator.py** -> to read xlsx tabel created by anomaly_db_generator and save images if the shower has anomaly score > threshold_score (for **showers**) or anomaly score < threshold_score (for **nsb**)

Also, project has example how to read *sim_telarray* simulation files **(*.simtel.gz)**: **read_simtelgz.ipynb**. And data flow [Data Flow Diagram](https://lucid.app/lucidchart/23ab3466-0dfa-4dcd-95ea-2716589799ce/edit?viewport_loc=-55%2C244%2C2219%2C1116%2C0_0&invitationId=inv_7e49648f-a868-4652-8443-2facc041a367#)

There are two scripts, describing how to use developed classes:
1. **create_anomaly_db_script.py** -> script to use **anomaly_db_generator** class and generate xlsx files with shower features and anomaly score;
2. **create_shower_image_script.py** -> script to use **image_db_generator** class and generate RGB images from xlsx file;

## Dependency:

In order to be able to convert hexagonal pixels into square, the [cta-pipe](https://cta-observatory.github.io/ctapipe/) and **dl1_data_handler** package is needed. 

Moreover, since this study was done for a future SiPM camera, which is not a part of CTA telescopes yet, Mykhailo Dalchenko updated the **dl1_data_handler** for our need. It can be found here: [dl1_data_handler](https://github.com/mexanick/dl1-data-handler/tree/sipm_inclusion). Please use branch use **sipm_inclusion** branch. 


Also, you you will need to modify ctapipe.instrument.guess.py and add the corresponding record in TELESCOPE_NAMES dictionary here:
[ctapipe.instrument.guess.py](https://github.com/cta-observatory/ctapipe/blob/15cd535e1025352de8477240cd3f8b5da1038147/ctapipe/instrument/guess.py#L19)
New record should be:
`GuessingKey(7987, 28.0): GuessingResult("LST", "SiPM", "LSTSiPMCam", 1),`

## Usage
To use the project, first **create_anomaly_db_script.py** script should be used to create *xlsx* files with shower features and anomaly score. After, **create_shower_image_script.py** script should be used to create RGB images from *xlsx* and sim_telarray output simulation files.

## Authors and acknowledgment
Developed by A. Nagai at University of Geneve, DPNC

## License
For open source projects, say how it is licensed.

