import os
from tqdm import tqdm
from ctapipe.io import EventSource, EventSeeker
from pycaret.anomaly import *
import pandas as pd
import numpy as np
from dl1_data_handler.image_mapper import ImageMapper
from PIL import Image

class anomaly_db:

    '''
    class developed to create the xlsx file with shower parameters (features)
    '''

    def __init__(self, name, id, file_to_ana, module_map, asum_pixel_map, n_ft_start_col, file_excel_out, n_input, shower_range = [33, 43]):
        '''
        :param n_input - n events to analyse
        :param name - session name
        :param id - session ID
        :param module_map - dictionary with pixels numbers inside a given camera module
        :param asum_pixel_map - dictionary with pixels numbers inside a given camera analog sum claster
        :param n_ft_start_col - integer number with colum number at which important numerical features starts
        :param file_excel_out - name of excel file where df of features and anomaly score save
        :param shower_range   - indexes range (at waveform) where shower signal is expected
        '''
        self.shower_range = shower_range
        self.silent_on = True
        self.session_name = name
        self.session_id = id
        self.n_events = n_input
        self.file = file_to_ana
        self.module_id_map = module_map
        self.asum_id_map = asum_pixel_map

        self.create_df_short()
        self.get_shower_param_short()
        self.data_frame = pd.DataFrame(self.data_dict)
        self.start_column_number = n_ft_start_col
        self.ignore_features_list = list(self.data_frame.columns[:self.start_column_number])

        self.numerical = list(self.data_frame.columns[self.start_column_number:])
        self.create_anomaly_df()
        print('save file : ', file_excel_out)
        self.anomaly_df.to_excel(file_excel_out, index=True)


    def create_anomaly_df(self):
        '''
        use isolatopn forest "iforest" as anomaly model to create anomaly score for each shower
        :return:
        '''

        exp_data = setup(self.data_frame, session_id=self.session_id, experiment_name=self.session_name, ignore_features=self.ignore_features_list, numeric_features=self.numerical, silent=self.silent_on)
        print('ignore features : ')
        for i_ignore_feature in self.ignore_features_list:
            print('              ', i_ignore_feature)
        print('=============================================')
        print('use features : ')
        for i_use_features in self.numerical:
            print('            ', i_use_features)
        print('=============================================')
        iforest_data = create_model('iforest')
        print(iforest_data)

        iforest_results_df = assign_model(iforest_data)
        self.anomaly_df = iforest_results_df


    def create_df_short(self):

        '''
        create empty data frame with a given features, see bellow:
        :return:
        '''

        data_df = {'file name': [], 'ID': [], 'interval': [], 'energy TeV.':[], 'pointing alt.': [], 'pointing az.': [],
                   'swhower alt.': [], 'swhower az.': [],
                   'pixel: charge max pos.': [], 'pixel: charge max significance': [],
                   'pixel: charge np significance > 3': [], 'pixel: gradient max significance': [],
                   'pixel: gradient np > std': [],
                   'module: charge max pos.': [], 'module: charge max significance': [],
                   'module: charge np > 2*std': [], 'module: gradient max significance': [],
                   'asum: max amplitude': [], 'asum: mean amplitude': [], 'asum: charge max pos.': [], 'asum: gradient max significance': [],
                   'asum: gradient np > 2*std': [], 'asum: gradient np > 3*std': [], };

        self.data_dict = data_df

    def get_shower_param_short(self):

        '''
        calculate shower parameters and fill the df with those parameters
        Important: self.shower_range define the time interval (at waveform) where the shower is expected. If set incorrectly, wrong parameters will be calculated
        :return:
        '''

        # df = pd.DataFrame(columns=['event id', 'interval', 'mean', 'max', 'max pos', 'std', 'np > std', 'np > 2*std', 'np > 3*std', 'np > 4*std', 'np > 5*std'])

        #n_point_interval = 10
        n_point_interval = self.shower_range[1] - self.shower_range[0]

        source = EventSource(self.file, max_events=self.n_events, back_seekable=True)
        seeker = EventSeeker(source)
        camgeom = source.subarray.tel[1].camera.geometry

        # for event in source:

        head, tail = os.path.split(self.file)

        #for i, event in tqdm(enumerate(source), total=self.n_events):
        for i, event in enumerate(source):

            waveform_length = len(event.r0.tel[1].waveform[0, 0])

            #interval = 3

            #p_start = interval * n_point_interval + 3
            #p_end = (interval + 1) * n_point_interval + 3
            
            p_start = self.shower_range[0]
            p_end = self.shower_range[1]
            charge_tmp = np.sum(event.r0.tel[1].waveform[0][:, p_start:p_end], axis=1)

            grad_max_val_tmp = np.max(np.gradient(event.r0.tel[1].waveform[0][:, p_start:p_end], axis=1, edge_order=1),
                                      axis=1)

            grad_mean_tmp = np.mean(grad_max_val_tmp)
            grad_std_tmp = np.std(grad_max_val_tmp)
            grad_max_tmp = np.max(grad_max_val_tmp)

            charge_mean_tmp = np.mean(charge_tmp)
            charge_std_tmp = np.std(charge_tmp)

            self.data_dict['file name'].append(tail)
            self.data_dict['ID'].append(event.index.event_id)
            self.data_dict['interval'].append(self.shower_range)

            #self.data_dict['energy TeV.'].append(float(str(event.simulation.shower.energy)[:-4]))

            if float(str(event.simulation.shower.energy)[:-4]) <= 1.e-10:
                str_start = self.file.find('muon_')
                str_end = self.file.find('GeV')
                energy_gev_tmp = float(self.file[str_start + 5:str_end])
                #print( self.file[str_start + 5:str_end])
                self.data_dict['energy TeV.'].append(energy_gev_tmp/1000.)
            else:
                self.data_dict['energy TeV.'].append(float(str(event.simulation.shower.energy)[:-4]))

            self.data_dict['pointing alt.'].append(float(str(event.pointing.tel[1].altitude)[:-3]))
            self.data_dict['pointing az.'].append(float(str(event.pointing.tel[1].azimuth)[:-3]))

            self.data_dict['swhower alt.'].append(float(str(event.simulation.shower.alt)[:-3]))
            self.data_dict['swhower az.'].append(float(str(event.simulation.shower.az)[:-3]))

            self.data_dict['pixel: charge max pos.'].append(np.argmax(charge_tmp))
            self.data_dict['pixel: charge max significance'].append((np.max(charge_tmp) - charge_mean_tmp) / charge_std_tmp)
            self.data_dict['pixel: charge np significance > 3'].append(
                np.count_nonzero((charge_tmp - charge_mean_tmp) / charge_std_tmp > 3))
            self.data_dict['pixel: gradient max significance'].append((grad_max_tmp - grad_mean_tmp) / grad_std_tmp)

            self.data_dict['pixel: gradient np > std'].append(
                np.count_nonzero(grad_max_val_tmp > grad_mean_tmp + grad_std_tmp))

            module_image = np.zeros(len(self.module_id_map.keys()))
            module_gradient_max_val = np.zeros(len(self.module_id_map.keys()))

            for module_id in range(len(self.module_id_map.keys())):
                pixel_start = self.module_id_map[str(module_id)][0]
                pisel_end = self.module_id_map[str(module_id)][-1]
                module_val_tmp = np.sum(event.r0.tel[1].waveform[0][pixel_start:pisel_end, p_start:p_end], axis=1)
                module_gradient_max_val[module_id] = np.max(
                    np.gradient(np.sum(event.r0.tel[1].waveform[0][pixel_start:pisel_end, p_start:p_end], axis=0),
                                axis=0, edge_order=0))
                module_image[module_id] = np.sum(module_val_tmp)

            module_mean = np.mean(module_image)
            module_std = np.std(module_image)
            module_max = np.max(module_image)

            module_grad_mean_tmp = np.mean(module_gradient_max_val)
            module_grad_std_tmp = np.std(module_gradient_max_val)
            module_grad_max_tmp = np.max(module_gradient_max_val)

            self.data_dict['module: charge max pos.'].append(np.argmax(module_image))
            self.data_dict['module: charge max significance'].append((module_max - module_mean) / module_std)

            self.data_dict['module: charge np > 2*std'].append(
                np.count_nonzero(module_image > module_mean + 2. * module_std))

            self.data_dict['module: gradient max significance'].append(
                (module_grad_max_tmp - module_grad_mean_tmp) / module_grad_std_tmp)

            asum_data = np.zeros(len(self.asum_id_map.keys()))
            asum_gradient_max_val = np.zeros(len(self.asum_id_map.keys()))
            asum_threshold = np.zeros(len(self.asum_id_map.keys()))

            for asum_id in range(len(self.asum_id_map.keys())):
                asum_waveform = np.zeros(n_point_interval)
                for pixel in self.asum_id_map[str(asum_id)]:
                    asum_data[asum_id] = asum_data[asum_id] + charge_tmp[pixel]
                    asum_waveform = asum_waveform + event.r0.tel[1].waveform[0][pixel, p_start:p_end]

                asum_gradient_max_val[asum_id] = np.max(np.gradient(asum_waveform, axis=0, edge_order=0))
                asum_threshold[asum_id] = np.max(asum_waveform) - np.mean(asum_waveform)
                # asum_gradient.append(np.gradient(asum_waveform, axis=0, edge_order=0))

            asum_grad_mean_tmp = np.mean(asum_gradient_max_val)
            asum_grad_std_tmp = np.std(asum_gradient_max_val)
            asum_grad_max_tmp = np.max(asum_gradient_max_val)

            asum_mean = np.mean(asum_data)
            asum_std = np.std(asum_data)
            asum_max = np.max(asum_data)

            self.data_dict['asum: max amplitude'].append(np.max(asum_threshold))
            self.data_dict['asum: mean amplitude'].append(asum_mean)

            self.data_dict['asum: charge max pos.'].append(np.argmax(asum_data))

            self.data_dict['asum: gradient max significance'].append(
                (asum_grad_max_tmp - asum_grad_mean_tmp) / asum_grad_std_tmp)

            self.data_dict['asum: gradient np > 2*std'].append(np.count_nonzero(asum_gradient_max_val > asum_grad_mean_tmp +
                                                                           2 * asum_grad_std_tmp))
            self.data_dict['asum: gradient np > 3*std'].append(np.count_nonzero(asum_gradient_max_val > asum_grad_mean_tmp +
                                                                           3 * asum_grad_std_tmp))
