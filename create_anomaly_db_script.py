import os
import sys, getopt
from pycaret.anomaly import *
from pycaret.classification import *

from camera_geometry import camera_geometry
from anomaly_db_generator import anomaly_db

# script is developed to read sim_telarray output file with showers and create xlsx file with shower parametets

# sim_telarray mapping pixel file:
mapping_file_name = "./input/camera_CTA-LST_SiPM_Ideal_analogsum21.dat"

camera = camera_geometry(mapping_file_name)
# get dictionary (camera.module_id_map) with key = modeule number (string), and value = pixes numbers  
camera.get_module_pixel_map()
# get dictionary (camera.asum_id_map) with key = analog sum claster number (string), and value = pixes numbers  
camera.get_analog_sum_trigger_map()

'''
 input parametets:
 -h - help
 -i - input sim_telarray array file (*.simtel.gz)
 -n - number of events in file to analyse
 -p - number of first features to not use for anomaly score calcualtion, usualy should be 8, so we ignore: 'file name', 'ID', 'interval', 'energy TeV.', 'pointing alt.', 'pointing az.', 'swhower alt.' and 'swhower az.'
 -o - puth to output folder
 -r - waveform index range where shower will be integrated to calculate the features (depends from sim_telarray simulation conditions)
'''

# read input parametets:
try:
    opts, args = getopt.getopt(sys.argv[1:], "hi:n:p:o:r:", ["ifile=", "events", "pass", "output", "range"])
except getopt.GetoptError:
    print('error: create_anomaly_db_script.py -i <inputfile> -n <n_events> -ignore <n_ignore> -o <output>  -r <range>')
    sys.exit(2)

n_events_to_ana = None
time_range = np.array([33, 43])

for opt, arg in opts:
    if opt == '-h':
        print('create_anomaly_db_script.py -i <inputfile> -n <n_events> -ignore <n_ignore> -o <output>  -r <range>')
        sys.exit()
    elif opt in ("-i", "--ifile"):
        input_file = arg
    elif opt in ("-n", "--events"):
        n_events_to_ana = int(arg)
    elif opt in ("-p", "--pass"):
        n_ft_start = int(arg)
    elif opt in ("-o", "--output"):
        output_path = arg
    elif opt in ("-r", "--range"):
        li_st = arg
        li_st = list(li_st.split(","))
        time_range[0] = int(li_st[0])
        time_range[1] = int(li_st[1])

if os.path.exists(input_file):
    # if input file set correctly:
    
    file_name_to_ana = os.path.basename(input_file)
    print('ana n events  : ', n_events_to_ana)
    print('file to start : ', file_name_to_ana)
    print('n ignore features: ', n_ft_start)
    print('output path : ', output_path)
    file_to_save = file_name_to_ana + '_n_events_' + str(n_events_to_ana) + '_df.xlsx'
    print('output file : ', file_to_save)
    
    start = file_name_to_ana.find('run') + 3
    end = file_name_to_ana.find('.simtel')
    run_number = int(file_name_to_ana[start:end])
    print('run_number : ', run_number)
    run_name = 'run_' + str(run_number)
    print('run_name   : ', run_name)
    print('shower integrate range   : ', time_range)


    # create xlsx file with features and anomaly score:
    df_generator = anomaly_db(run_name, run_number, input_file, camera.module_id_map, camera.asum_id_map, n_ft_start, output_path + "/" + file_to_save, n_events_to_ana, shower_range = time_range)


else:
    # if input file set un-correctly:
    print('file not exist')
    print('ana n events  : ', n_events_to_ana)
    print('file to start : ', input_file)
    print('n ignore features: ', n_ft_start)

