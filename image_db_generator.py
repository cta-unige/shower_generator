import os
from tqdm import tqdm
from ctapipe.io import EventSource, EventSeeker
from pycaret.anomaly import *
import pandas as pd
import numpy as np
from dl1_data_handler.image_mapper import ImageMapper
from PIL import Image

class image_db:

    '''
    Class developed to create images from sim_telarray files
    Only showers with anomaly score > threshold value will be converted into images if  do_nsb = False
    and only Only showers with anomaly score < threshold value will be converted into images if  do_nsb = True
    '''

    def __init__(self, path_df, path_corsica, output_path, anomaly_thr, particle_type, do_nsb, n_input, peak_pos = 38, step = 2):
        '''

        :param path_df: path to xlsx file with shower features and anomaly score
        :param path_corsica: path to FOLDER with all sim_telarray files
        :param output_path:  path where save images, should contain such sub folders: "clipped" "normalized" and "original"
        :param anomaly_thr:  threshold for anomaly score to separeate NSB and shower
        :param particle_type: type of particle (i.e. electron, gamma on, gamma diffused, proton or muon), will be used at file name
        :param do_nsb:    do we need images for Showers or NSB, it define if we use showers with anomaly score higher than threshold or lower than
        :param n_input:   n events to analyse
        :param peak_pos: time bin where shower maximum is expected
        :param step: each image will have width of 2 x step, Since we have 3 images, total time width which will be covered is: 3x 2x step
        '''

        self.n_events = n_input
        self.peak_pos = peak_pos
        self.step = step
        self.data_frame = pd.read_excel(path_df, index_col=0)
        self.get_corsica_file_name(path_corsica)
        self.get_run_number()
        self.output_general = output_path
        self.anomaly_score_threshold = anomaly_thr
        self.do_nsb = do_nsb
        self.particle = particle_type
        self.get_shower_ids()
        print('do save')
        self.save_square_images()

    def get_corsica_file_name(self, path_corsica):
        '''
        Create sim_telarray file name
        :param path_corsica:
        '''
        self.corsica_file_name = path_corsica + self.data_frame['file name'][0]
        print('file name : ', self.corsica_file_name)

    def get_run_number(self):
        '''
        Get run number from file name
        '''
        start = self.corsica_file_name.find('run') + 3
        end = self.corsica_file_name.find('.simtel')
        self.run_number = int(self.corsica_file_name[start:end])

    def get_shower_ids(self):
        '''
        Get shower IDs which will be converted into images
        '''
        if self.do_nsb == False:
            self.ids = self.data_frame[self.data_frame['Anomaly_Score'] > self.anomaly_score_threshold]['ID']
        else:
            self.ids = self.data_frame[self.data_frame['Anomaly_Score'] < self.anomaly_score_threshold]['ID']

    def save_square_images(self):
        '''
        Save square images, it containes three images one codded into R - red color, G - green color and B - blue color

        :param peak_pos: time bin where shower is expected
        :param step: each image will have width of 2 x step, Since we have 3 images, total time width which will be covered is: 3x 2x step
        :return:
        '''

        print('corsica file : ', self.corsica_file_name)
        eventsource = EventSource(self.corsica_file_name, max_events=1)
        seeker = EventSeeker(eventsource)

        for event in eventsource:
            print(event.index)

            cam_geom = eventsource.subarray.tel[1].camera.geometry
            cam_name = cam_geom.camera_name

        default_mapper = ImageMapper(camera_types=[cam_name,], camera_geometries={cam_name:cam_geom,})

        print('peak : ', self.peak_pos, ' stet : ', self.step)

        eventsource = EventSource(self.corsica_file_name, max_events=self.n_events)
        seeker = EventSeeker(eventsource)

        n_showers = 0
        for i, event in enumerate(eventsource):

            ID = event.index.event_id

            shower = False

            for id_current in self.ids:
                if id_current == event.index.event_id:
                    shower = True
                    n_showers = n_showers + 1
                    break


            if shower == True:

                print("shower ID : ", ID)
                waveform = event.r0.tel[1]['waveform'].T

                image_orig = np.max(waveform[self.peak_pos-self.step:self.peak_pos+self.step,:,:], axis=0)

                image_orig_bf = np.sum(waveform[self.peak_pos-3*self.step:self.peak_pos-self.step,:,:], axis=0)
                image_orig_nx = np.sum(waveform[self.peak_pos+self.step:self.peak_pos+3*self.step,:,:], axis=0)

                self.image = default_mapper.map_image(image_orig, cam_name)
                self.image_bf = default_mapper.map_image(image_orig_bf, cam_name)
                self.image_nx = default_mapper.map_image(image_orig_nx, cam_name)

                #plot_image(image)
                image_name = self.output_general + '/original/' + self.particle + '_run_' + str(self.run_number) + '_id_' + str(ID) + '.jpg'
                image_name_norm = self.output_general + '/normalized/normalized_' + self.particle + '_run_' + str(self.run_number) + '_id_' + str(ID) + '.jpg'
                image_name_clipped = self.output_general + '/clipped/clipped_' + self.particle + '_run_' + str(self.run_number) + '_id_' + str(ID) + '.jpg'

                self.conv_to_image(image_name_norm)
                self.conv_to_image_original(image_name)
                self.conv_to_image_clipp(image_name_clipped)


        if n_showers == len(self.ids):
            print("found : ",n_showers, " showers, expected : ",len(self.ids))

    def conv_to_image_original(self, name):
        '''
        convert intensity map into image. Since our camera has 12 bit resolution and RGB only 8 bits, the intensity will be converted with factor norm_factor = 255 / 4092
        :param name: path + name, how image will be saved
        :return:
        '''

        data = np.zeros((self.image.shape[0], self.image.shape[1], 3), dtype=np.uint8)
        norm_factor = 255 / 4092
        data[:, :, 0] = self.image_bf[:, :, 0] * norm_factor
        data[:, :, 1] = self.image[:, :, 0] * norm_factor
        data[:, :, 2] = self.image_nx[:, :, 0] * norm_factor

        img = Image.fromarray(data, 'RGB')
        img.save(name)

    def conv_to_image(self, name):

        '''
         convert intensity map into image.
         Since our camera has 12 bit resolution and RGB only 8 bits, the intensity will be converted with factor norm_factor = 255 / max_Intensity
        :param name:
        :return:
        '''

        data = np.zeros((self.image.shape[0], self.image.shape[1], 3), dtype=np.uint8)
        data[:, :, 0] = self.image_bf[:, :, 0] * (255 / np.max(self.image_bf[:, :, 0]))
        data[:, :, 1] = self.image[:, :, 0] * (255 / np.max(self.image[:, :, 0]))
        data[:, :, 2] = self.image_nx[:, :, 0] * (255 / np.max(self.image_nx[:, :, 0]))

        img = Image.fromarray(data, 'RGB')
        img.save(name)

    def conv_to_image_clipp(self, name):

        '''
        Convert intensity map into image.
        Since our camera has 12 bit resolution and RGB only 8 bits, the intensity will be clipped.
        For this we subtract mean value and clipp it to 255
        :param name:
        :return:
        '''

        data = np.zeros((self.image.shape[0], self.image.shape[1], 3), dtype=np.uint8)

        data_bf =self.image_bf
        data_mean = self.image
        data_nx = self.image_nx

        data_bf[:, :, 0] = self.image_bf[:, :, 0] - np.mean(self.image_bf[:, :, 0])
        data_mean[:, :, 0] = self.image[:, :, 0] - np.mean(self.image[:, :, 0])
        data_nx[:, :, 0] = self.image_nx[:, :, 0] - np.mean(self.image_nx[:, :, 0])

        data_bf[:, :, 0] = np.clip(data_bf[:, :, 0], 0, 255)
        data_mean[:, :, 0] = np.clip(data_mean[:, :, 0], 0, 255)
        data_nx[:, :, 0] = np.clip(data_nx[:, :, 0], 0, 255)

        data[:, :, 0] = np.clip(data_bf[:, :, 0] - np.mean(data_bf[:, :, 0]), 0, 255)
        data[:, :, 1] = np.clip(data_mean[:, :, 0] - np.mean(data_mean[:, :, 0]), 0, 255)
        data[:, :, 2] = np.clip(data_nx[:, :, 0] - np.mean(data_nx[:, :, 0]), 0, 255)

        img = Image.fromarray(data, 'RGB')
        img.save(name)
        # img.show()


