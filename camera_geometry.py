import os
from tqdm import tqdm
from ctapipe.io import EventSource, EventSeeker
from pycaret.anomaly import *
import pandas as pd
import numpy as np
from dl1_data_handler.image_mapper import ImageMapper
from PIL import Image

class camera_geometry:

    '''
    Class developed to get camera geometry from sim_telarray file
    '''
    def __init__(self, geometry_file):
        '''

        :param geometry_file: File with the list of pixels per each module (from sim_telarray simulation)
        '''
        self.map_file = geometry_file

    def get_module_pixel_map(self):
        '''
        Implements mapping relation between pixels and trigger modules
        :return: Dictionary {'module_id_map':[pixel ids]}
        '''
        module_pixel_map = {}

        with open(self.map_file, "r") as mapping_file:
            lines = mapping_file.readlines()
            for line in lines:
                if ((line.find('Pixel') != -1) and (line.find('Pixel format:') == -1)) and (line.find('#') == -1):
                    words = line.split()
                    # print(line)
                    pixel_id = int(words[1])
                    module_id = words[5]
                    try:
                        tmp_pixel_list = module_pixel_map[module_id]
                        tmp_pixel_list.append(pixel_id)
                    except KeyError:
                        tmp_pixel_list = [pixel_id]
                    module_pixel_map[module_id] = tmp_pixel_list
        self.module_id_map = module_pixel_map

    def get_analog_sum_trigger_map(self):
        '''
        Implements mapping relation between pixels and trigger modules
        :return: Dictionary {'ModuleID':[pixel ids]}
        '''
        asum_pixel_map = {}
        asum_id = 0

        with open(self.map_file, "r") as mapping_file:
            lines = mapping_file.readlines()
            for line in lines:
                if ((line.find('AnalogSumTrigger') != -1) and (line.find('#') == -1)):
                    words = line.split()
                    # print(words[0], ' ', words[1], ' ', words[2], ' ', words[3])
                    for word in words[3:]:
                        # print(asum_id, ' ', word)
                        pixel_id = int(word)

                        # module_id = words[5]
                        try:
                            tmp_pixel_list = asum_pixel_map[str(asum_id)]
                            tmp_pixel_list.append(pixel_id)
                        except KeyError:
                            tmp_pixel_list = [pixel_id]
                        asum_pixel_map[str(asum_id)] = tmp_pixel_list
                    asum_id = asum_id + 1
        self.asum_id_map = asum_pixel_map 
